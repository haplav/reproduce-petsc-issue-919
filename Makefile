CFLAGS           =
FFLAGS           =
CPPFLAGS         =
FPPFLAGS         =
LOCDIR           = src/ksp/ksp/examples/tutorials/
DIRS             = network
MANSEC           = KSP
CLEANFILES       = rhs.vtk solution.vtk
NP               = 1

.PHONY: build clean run_1k run_ecology2

build: petscHypreSolver

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules
include ${PETSC_DIR}/lib/petsc/conf/test

run_1k: petscHypreSolver
	mpiexec -n 1 ./petscHypreSolver -f ecology2_1k_petsc.mat -ksp_type cg -pc_type hypre -pc_hypre_type boomeramg -ksp_rtol 1e-8

run_ecology2: petscHypreSolver
	mpiexec -n 1 ./petscHypreSolver -f ecology2_petsc.mat -ksp_type cg -pc_type hypre -pc_hypre_type boomeramg -ksp_rtol 1e-8

clean::
	$(RM) petscHypreSolver
